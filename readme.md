# Benchmarking Big Data Infrastructure
Welcome to my Master's Thesis public repository. Here, you will find all resources described in the thesis that enable you to repeat the benchmarks. I've also added some helper scripts that facilitate extraction and presentation of the retrieved data.

__Note:__ The readme file seems to be a sprinter. Please give the other files a few days to reach this server.

### Contents
- /
	- `readme`
	- `masterthesis_striffeler.pdf` This PDF describes all the steps to benchmark Hadoop clusters in detail.
	- /benchmarking
		- `run-benchmarks.sh` A script to run a selection of Hadoop benchmarks.
		- `example.conf` Example configuration. You will certainly have to adapt the informations, at least those concerning user and path information.
	- /evaluation
		- `metrics_extractor.py` This python script will extract csv files from the Ambari metrics collected during the benchmark runs. It will directoy create some neat plots of this data using the plot-`metrics-single.pl` template.
		- `plot-all.pl` I used this gnuplot script to generate all plots you find in the paper.
	- /utilities
		- `parse-testdfsio.sh` this little script helps you extracting the TestDFSIO results from results.out and directly copies the numbers to your clipboard. You can then e.g. paste them in a spreadsheet. 
		- `get-swim-results.sh` Extracts job durations from all swim jobs and writes them to a single file. Helpful for aggregating the results, e.g. in a spreadsheet.
		- `killAllRunningHadoopJobs.sh` Useful if you run your tests on a cluster that is used by others at office hours. Installing this script as a cronjob allows you to abort all your benchmarking load when others start working.


### Notes
- You should consider using a mechanism that ensures that your script keeps running even if your connection to the server is interrupted. For instance, you could use screen or nohup, while the former is the more comfortable way in my opition.
