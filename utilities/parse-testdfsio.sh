#!/bin/bash


# TestDFSIO write
while read line; do
	if [[ $line == *"Throughput mb/sec:"* ]]; then
		O11="$(echo $line | awk -F': ' '{print $2}')"
		#echo -en $line | awk -F': ' '{print $2}'
	elif [[ $line == *"Average IO rate mb/sec:"* ]]; then
		O12="$(echo $line | awk -F': ' '{print $2}')"
		#echo -en "\t" && echo -en$line | awk -F': ' '{print $2}'
	elif [[ $line == *"IO rate std deviation:"* ]]; then
		O13="$(echo $line | awk -F': ' '{print $2}')"
		#echo $line | awk -F': ' '{print $2}'
	elif [[ $line == *"Test exec time sec:"* ]]; then
		O14="$(echo $line | awk -F': ' '{print $2}')"
		#echo $line | awk -F': ' '{print $2}'
	fi
done < testdfsio_write.out

# TestDFSIO read
while read line; do
	if [[ $line == *"Throughput mb/sec:"* ]]; then
		O21="$(echo $line | awk -F': ' '{print $2}')"
		#echo -en $line | awk -F': ' '{print $2}'
	elif [[ $line == *"Average IO rate mb/sec:"* ]]; then
		O22="$(echo $line | awk -F': ' '{print $2}')"
		#echo -en "\t" && echo -en$line | awk -F': ' '{print $2}'
	elif [[ $line == *"IO rate std deviation:"* ]]; then
		O23="$(echo $line | awk -F': ' '{print $2}')"
		#echo $line | awk -F': ' '{print $2}'
	elif [[ $line == *"Test exec time sec:"* ]]; then
		O24="$(echo $line | awk -F': ' '{print $2}')"
		#echo $line | awk -F': ' '{print $2}'
	fi
done < testdfsio_read.out

echo -e "$O11\t$O12\t$O13\t$O14\t$O21\t$O22\t$O23\t$O24" > results-parsed.out
cat results-parsed.out | xclip -selection c
