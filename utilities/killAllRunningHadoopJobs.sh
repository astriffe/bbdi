#!/bin/bash

# NOTE: Run cronjob every weekday at 7am: crontab -e
#	00 07 * * 1-5 /home/astriffeler/killAllRunningHadoopJobs.sh

# Task: Kill all running Hadoop jobs using 'mapred job'
activejobs=$(mapred job -list 2>/dev/null | grep '^\ job' | while read -r line; do echo $line | awk -F '\ ' '{print $1}' ; done)

for i in $activejobs; do
	mapred job -kill $i
done
