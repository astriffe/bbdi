#!/bin/bash

#===================================================================================================
#	Script for executing a selection of different Hadoop benchmarks. See README.md #TODO for more details.
#	@author Alexander Striffeler
#	@date	2015-06-22
#
#	Prerequisitories:	
#		- Hadoop is up and running
#		- All variables are set to the system-specific values (possibily with config file)
#		- Git, Javac, gcc are installed.
#		- Java Version 1.7 available
#
#	Caution:
#		- The benchmarks will write into hdfs:/user/$USER/benchmarks and delete this directory once 
#			the scripts ends.
#		- This script requires access to both hdfs:/user/$USER/benchmarks as well as to 
#			hdfs://benchmarks
#
#	NB:
#		- The replication factor loops will only give meaningful output	on standard HDFS
#			installations. File systems such as Isilon or NDFS (Nutanix) will provide their own 
#			replication/security mechanisms. Therefore, the script only uses replication factor 3 
#			by default.
#===================================================================================================



#---Variables---------------------------------------------------------------------------------------
OPTIND=1         # Reset in case getopts has been used previously in the shell.


#---Default Config----------------------------------------------------------------------------------

## 
## NB: User and path information is usually set in the configuration files.
##

USER="alex"

HISTORY_SERVER='diufpc110:19888'
HADOOP_TEST_HOME="/usr/hdp/2.2.6.0-2800/hadoop-mapreduce" # This holds for cloudera, installed using parcels.
HADOOP_HOME="/usr/hdp/2.2.6.0-2800/hadoop" # Find it using ${hadoop version} and only extract the path, i.e., insert the directory which contains hadoop-common-*.jar. Do not add tailing slash.

# Metric collection
AMBARI_SERVER="diufpc110"
A_USERNAME="metric"
A_PASSWORD="metric"
CLUSTERNAME="as_minicluster"


# Selective runs - run all by default.
RUN_TESTDFSIO=true
RUN_PI=true
RUN_TERASORT=true
RUN_MRBENCH=true
RUN_NNBENCH=true
RUN_HIVE=true
RUN_SWIM=true

# Generic
FOLDER="benchmark_results" # Local output folder name. Will be prefixed with hostname and timestamp. Subfolder of $HOME.
OUT_DIR="${HOME}/$FOLDER"
DFS_DIR="/user/$USER/benchmarks" # MRBench does not respect the parameters and hence additionally requires access to hdfs:/benchmarks

REPLICATION_FACTORS="3" #"1 3 5" # List of all replication factors to be tested by TestDFSIO and NNBench
HISTORY_SERVER_TIMEOUT=10 # The duration in sec to wait for the history server to serve the job information once a job finished.

# TestDFSIO
TESTDFSIO_NOFILES=100
TESTDFSIO_FILESIZE=1GB

# Pi
BBP_STARTDIGIT=1
BBP_NODIGITS=500000 # has to be multiple of 4
BBP_NMAPS=128

# Terasort
TERASORT_SIZE=1000000000 #100GB
TS_TASKS=12

# MRBench
MR_NUMRUNS=100

# NNBench
NN_NOFILES=100000

# Hive-testbench
HIVE_SCALE=8

# SWIM
NO_MACHINES=4			# Number of machines in the cluster.
LIMIT_SWIM_LINES=1000	# Only first 1000 jobs by default.


#---Functions---------------------------------------------------------------------------------------
timestamp() {
	date +"%Y-%m-%d %H:%M:%S.%N"
}

# Expects parameters:
#	$1	start epoch
#	$2	end epoch
#	$3	run description	
getstats() {
	curl -u $A_USERNAME:$A_PASSWORD --globoff http://$AMBARI_SERVER:8080/api/v1/clusters/$CLUSTERNAME?fields=metrics[$1,$2,1] >> $METRICS/$3.dat
}

show_help() {
	echo -e 'This script runs a set of benchmarks on Hadoop distributions like Cloudera and Hortonworks. Please make sure to set all mandatory parameters, such as path information. All parameters that are not set, will stick to reasonable default values. Please pay attention to the requirements of the program listed in the README file or in the header of the script itself.\nUsage: benchmarks_run.sh [-c config_file] [-d run_description] [-h] \n\t-h, -?: Show this dialog\n\t-c <file>: Provide a file to set custom parameters.\n\t-d <description>: Enter a description of the run, which will be written to the results file.'
}


#---Getopts-----------------------------------------------------------------------------------------
while getopts "h?c:d:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    c)  
		CONFIG_FILE=$OPTARG
        ;;
        
    d)
    	DESCRIPTION=$OPTARG
    	;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ -n "$CONFIG_FILE" ]; then
	if [ ! -f $CONFIG_FILE ]; then
		echo -e "ERROR: File $CONFIG_FILE could not be found."
		exit 1
	fi
	echo -e "INFO\tLoading custom configuration: $CONFIG_FILE."
	source $CONFIG_FILE
fi


#---Prereq check------------------------------------------------------------------------------------
if [ -z $USER -o -z $HISTORY_SERVER -o -z $HADOOP_TEST_HOME -o -z $HADOOP_HOME -o -z $NO_MACHINES ]; then
	echo -e "The following properties are required:\n\tUSER ("$USER")\n\tHISTORY_SERVER ("$HISTORY_SERVER")\n\tHADOOP_TEST_HOME ("$HADOOP_TEST_HOME")\n\tHADOOP_HOME ("$HADOOP_HOME")\n\tNO_MACHINES ("$NO_MACHINES")"
	ABORT=true
fi


#---Some system checks / setup first----------------------------------------------------------------
mkdir -p $OUT_DIR


#	Check environment
ABORT=false
type gcc >/dev/null 2>&1 || { echo -e >&2 $(timestamp)"\tERROR\tI require gcc but it's not installed."; ABORT=true; }
type javac >/dev/null 2>&1 || { echo -e >&2 $(timestamp)"\tERROR\tI require javac but it's not installed."; ABORT=true; }
type git >/dev/null 2>&1 || { echo -e >&2 $(timestamp)"\tERROR\tI require git but it's not installed."; ABORT=true; }

#	Check Java version
if type -p java; then
    _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    _java="$JAVA_HOME/bin/java"
else
    echo "No Java installation found."
    ABORT=true
fi

if [[ "$_java" ]]; then
    version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    if [[ ! "$version" > "1.6" ]]; then
        "The SWIM Benchmark requires Java 1.7."
        ABORT=true
    fi
fi

#	Check whether Hadoop home directory is set.
if [[ -z "$HADOOP_TEST_HOME" ]]; then
	echo -e $(timestamp)"\tERROR:\tVariable HADOOP_TEST_HOME is not set." 
	ABORT=true
fi

# 	Check whether home directory for user exists.
if [ ! -d "/home/"$USER ]; then
	echo -e $(timestamp)"\tERROR:\tHome directory for user "$USER" does not exist."
	ABORT=true
fi

#	Check whether user can access and write to  hdfs:/benchmarks
if [ -n "$(hdfs dfs -mkdir -p /benchmarks/deleteme-imfortestingonly)" ]; then
	echo -e $(timestamp)"\tERROR:\tUser $USER does not have permissions to write into HDFS directory /benchmarks or /benchmarks directory does not exist. This however is required for MRBench."
	ABORT=true
else
	hdfs dfs -rmdir /benchmarks/deleteme-imfortestingonly
fi

#	Abort if above requirements are not met.
if [ "$ABORT" == true ]; then
	echo "Aborting."
	exit 1
fi

#	Create local file structure.
mkdir -p $OUT_DIR
cd $OUT_DIR

OUT=${OUT_DIR}'/log.out'
RES=${OUT_DIR}'/results.out'
METRICS=${OUT_DIR}'/metrics'

# Create log file (first write into will happen rather late, non-existence may be confusing.)
touch $OUT
mkdir -p $METRICS



#	Start Log with machine and time data
echo -e "\n===================================================\n"$(timestamp)"\tINFO\tStarting new run..." >> $RES
echo -e "\t\t\t\t\tHostname:\t$(hostname -f)" >> $RES
if [ -n "$CONFIG_FILE" ]; then
	echo -e "\t\t\t\t\tConfiguration:\t$CONFIG_FILE" >> $RES
	cp $HOME/$CONFIG_FILE $OUT_DIR/current_config.conf 	# Copy external config to result directory for later reference.
fi
if [ -n "$DESCRIPTION" ]; then
	echo -e "\t\t\t\t\tDescription:\t$DESCRIPTION" >> $RES
fi
echo -e "\n\nScheduled benchmarks:" >> $RES
for b in "RUN_TESTDFSIO" "RUN_PI" "RUN_TERASORT" "RUN_MRBENCH" "RUN_NNBENCH" "RUN_HIVE" "RUN_SWIM"; do
	echo -e "\t\t\t\t\t$b : ${!b}" >> $RES
done

echo -e "\n===================================================\n" >> $RES

#---Benchmark DFS IO performance--------------------------------------------------------------------
#		TestDFSIO
#		Parameters:	-nrFiles 100 -size 1GB -resFile '/home/hdfs/testdfsio.out' 
if ($RUN_TESTDFSIO); then
	echo -e "***\n"$(timestamp)"\tINFO\tEntering TestDFSIO benchmark" >> $RES

	for i in $REPLICATION_FACTORS; # Define replication factor(s), only applied in write-operation obviously but read-performance could benefit.
	do
		echo -e $(timestamp)"\tINFO\tNEW RUN, replication factor $i" >> $RES
		#	Write
		echo -e $(timestamp)"\tINFO\tStarting TestDFSIO WRITE" >> $RES
		START_DFSIO_WRITE=$(date +%s)
		hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar TestDFSIO -Dtest.build.data=$DFS_DIR/testdfsio $EXEC_OPTIONS -Ddfs.replication=$i -write -nrFiles $TESTDFSIO_NOFILES -size $TESTDFSIO_FILESIZE -resFile $OUT_DIR/testdfsio_write.out >> $OUT 2>&1 

		getstats $START_DFSIO_WRITE $(date +%s) "testdfsio-write-$i"
		tail -n 9 $OUT_DIR/testdfsio_write.out >> $RES

		#	Read
		echo -e $(timestamp)"\tINFO\tStarting TestDFSIO READ" >> $RES
		START_DFSIO_READ=$(date +%s)
		hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar TestDFSIO -Dtest.build.data=$DFS_DIR/testdfsio $EXEC_OPTIONS -read -nrFiles $TESTDFSIO_NOFILES -size $TESTDFSIO_FILESIZE -resFile "$OUT_DIR/testdfsio_read.out" >> $OUT_DIR/dfsio_read.log 2>&1 
		getstats $START_DFSIO_READ $(date +%s) "testdfsio-read-$i"
		tail -n 9 $OUT_DIR/testdfsio_read.out >> $RES

		#	Cleanup
		echo -e $(timestamp)"\tINFO\tStarting TestDFSIO Cleaning up" >> $RES
		hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar TestDFSIO -Dtest.build.data=$DFS_DIR/testdfsio $EXEC_OPTIONS -clean -nrFiles $TESTDFSIO_NOFILES -size $TESTDFSIO_FILESIZE -resFile "$OUT_DIR/testdfsio_cleanup.out">> $OUT 2>&1
	
	done
fi

#---Pi Benchmark------------------------------------------------------------------------------------
#	Directory /user/$USER/bbp must not exist, otherwise the benchmark aborts (see log.out)
#	Parameters: 	start digit		1
#					No digits		100'000
#					Maps			64
if ($RUN_PI); then
	echo -e "***\n"$(timestamp)"\tINFO\tEntering Pi benchmark\n" >> $RES
	echo "PI Params: $BBP_STARTDIGIT / $BBP_NODIGITS / $BBP_NMAPS" >> $RES
	PI_START=$(date +%s)
	hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-examples.jar bbp $EXEC_OPTIONS $BBP_STARTDIGIT $BBP_NODIGITS $BBP_NMAPS /user/$USER/benchmarks/bbp 1>> $RES 2>> $OUT
	getstats $PI_START $(date +%s) "pi-benchmark"

	hdfs dfs -rm -r -skipTrash /user/$USER/benchmarks/bbp >> $OUT	
fi

#---TeraSort benchmark------------------------------------------------------------------------------
if ($RUN_TERASORT); then
	echo -e "***\n"$(timestamp)"\tINFO\tEntering Terasort benchmark\n" >> $RES

	#	Generate directories
	hdfs dfs -mkdir /user/$USER/benchmarks/terasort >> $OUT

	#	Create data using teragen
	echo -e $(timestamp)"\tEVENT\tStarting Teragen" >> $RES
	START_TERAGEN=$(date +%s)
	hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-examples.jar teragen -Dmapred.map.tasks=$TS_TASKS $EXEC_OPTIONS $TERASORT_SIZE /user/$USER/benchmarks/terasort/teragen-out >> $OUT 2>&1
	getstats $START_TERAGEN $(date +%s) "teragen"
	echo -e $(timestamp)"\tEVENT\tTeragen done" >> $RES

	## 	Assuming exclusive access to the cluster (at least for the specific user), we can cheat to get 
	#	the jobhistory file we need since TeraSort does not write own job statistics.
	sleep $HISTORY_SERVER_TIMEOUT		# Wait for the HistoryServer to copy the file from intermediate to done jobs. 

	#	1. get job name
	JOBNAME=$(tac $OUT | grep -m 1 "mapreduce.Job: Running job: job_" | awk '{print $7;}')

	#	2. download job statistics
	curl "http://$HISTORY_SERVER/ws/v1/history/mapreduce/jobs/$JOBNAME" >> $RES 2>> $OUT
	echo -e "\n" >> $RES

	#	Terasort
	echo -e $(timestamp)"\tEVENT\tStarting Terasort" >> $RES
	START_TERASORT=$(date +%s)
	hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-examples.jar terasort -Dmapred.reduce.tasks=$TS_TASKS $EXEC_OPTIONS /user/$USER/benchmarks/terasort/teragen-out /user/$USER/benchmarks/terasort/terasort-out >> $OUT 2>&1
	getstats $START_TERASORT $(date +%s) "terasort"
	echo -e $(timestamp)"\tEVENT\tTerasort done" >> $RES

	sleep $HISTORY_SERVER_TIMEOUT		# Wait for the HistoryServer to copy the file from intermediate to done jobs. 
	#	1. get job name
	JOBNAME=$(tac $OUT | grep -m 1 "mapreduce.Job: Running job: job_" | awk '{print $7;}')

	#	2. download job statistics
	curl "http://$HISTORY_SERVER/ws/v1/history/mapreduce/jobs/$JOBNAME" >> $RES 2>> $OUT
	echo -e "\n" >> $RES

	##	Validate
	echo -e $(timestamp)"\tEVENT\tStarting Teravalidate" >> $RES
	hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-examples.jar teravalidate $EXEC_OPTIONS /user/$USER/benchmarks/terasort/terasort-out /user/$USER/benchmarks/terasort/teravalidate-out >> $OUT 2>&1
	echo -e $(timestamp)"\tEVENT\tTeravalidate done" >> $RES

	sleep $HISTORY_SERVER_TIMEOUT		# Wait for the HistoryServer to copy the file from intermediate to done jobs. 
	#	1. get job name
	JOBNAME=$(tac $OUT | grep -m 1 "mapreduce.Job: Running job: job_" | awk '{print $7;}')

	#	2. download job statistics
	curl "http://$HISTORY_SERVER/ws/v1/history/mapreduce/jobs/$JOBNAME" >> $RES 2>> $OUT
	echo -e "\n" >> $RES

	#	Remove generated data
	hdfs dfs -rm -r -skipTrash /user/$USER/benchmarks/terasort >> $OUT	

fi

#---MRBench-----------------------------------------------------------------------------------------
if ($RUN_MRBENCH); then
	echo -e $(timestamp)"\tEVENT\tStarting MRBench" >> $RES
	START_MRBENCH=$(date +%s)
	hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar mrbench $EXEC_OPTIONS -baseDir /user/$USER/benchmarks/mrbench -numRuns $MR_NUMRUNS -maps 10 -reduces 1 -inputLines 5 -inputType random >> $RES 2>> $OUT
	getstats $START_MRBENCH $(date +%s) "mrbench"

	hdfs dfs -rm -r -skipTrash /benchmarks/MRBench
fi

#---NNBench-----------------------------------------------------------------------------------------
#	-blockSize must match the property dfs.namenode.fs-limits.min-block-size.
#	-Benchmark writes to /benchmarks/nnbench.

if ($RUN_NNBENCH); then
	echo -e $(timestamp)"\tEVENT\tStarting NNBench" >> $RES

	for i in $REPLICATION_FACTORS;
	do
		echo -e $(timestamp)"\tINFO\tNEW RUN, Replication factor $i" >> $RES
		START_NNBENCH=$(date +%s)
		hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar nnbench -operation create_write -blockSize 1048576 -bytesToWrite 10 -maps 16 -reduces 1 -numberOfFiles $NN_NOFILES -replicationFactorPerFile $i >> $OUT 2>&1
		getstats $START_NNBENCH $(date +%s) "nnbench-create_write-$i"
		tail -n 30 $OUT >> $RES

		for op in {"open_read","rename","delete"};
		do
			START_NN=$(date +%s)
			hadoop jar $HADOOP_TEST_HOME/hadoop-mapreduce-client-jobclient-tests.jar nnbench -operation $op -blockSize 1048576 -bytesToWrite 10 -maps 16 -reduces 1 -numberOfFiles $NN_NOFILES >> $OUT 2>&1
			getstats $START_NN $(date +%s) "nnbench-$op-$i"
			tail -n 30 $OUT >> $RES
		done
	done

	#	Remove directory
	hdfs dfs -rm -r -skipTrash /benchmarks/nnbench >> $OUT 2>&1
fi

#---TPC-H benchmark---------------------------------------------------------------------------------
if ($RUN_HIVE); then
	echo -e $(timestamp)"\tEVENT\tTPC-H Setup" >> $RES
	if [ ! -d "./hive-testbench" ]; then
		git clone https://github.com/hortonworks/hive-testbench.git hive-testbench/ >> $OUT 2>&1
	fi
	cd hive-testbench

	./tpch-build.sh >> $OUT 2>&1
	./tpch-setup.sh $HIVE_SCALE >> $OUT 2>&1 # Generate data
	
	# Some properties in the default configuration files stop hive shell from starting. Remove them.
	sed -i '/^set hive.auto.convert.sortmerge.join.noconditionaltask=/d' */testbench.settings
	sed -i '/^set hive.optimize.mapjoin.mapreduce=/d' */testbench.settings
	sed -i '/^set hive.semantic.analyzer.factory.impl=/d' */testbench.settings
	# Some others have to be adapted...
	sed -i 's/-- set hive.tez./set hive.tez./g' */testbench.settings
	echo "set mapreduce.child.java.opts=-Xmx4G -XX:+UseConcMarkSweepGC;" | tee -a */testbench.settings
	sed -i 's/set hive.auto.convert.join=true/set hive.auto.convert.join=false/g' */testbench.settings	
	# Set task timeout to 1 hour (measured in ms)
	echo "set mapreduce.task.timeout=3600000;" | tee -a */testbench.settings 

	# Adapt TPCH Query19 according to https://issues.apache.org/jira/secure/attachment/12416257/TPC-H_on_Hive_2009-08-11.pdf
	sed -i 's/^\tlineitem,$/\tlineitem l join part p on p.p_partkey = l.l_partkey/g' sample-queries-tpch/tpch_query19.sql
	sed -i 's/^\tpart$//g' sample-queries-tpch/tpch_query19.sql

	echo -e $(timestamp)"\tINFO\tTPC-H Results following" >> $RES
	TPCH_START=$(date +%s)
	for i in `seq 1 22`;
	do
		BEFORE=$(date +"%s")
		hive -i sample-queries-tpch/testbench.settings -e "USE tpch_flat_orc_$HIVE_SCALE;source sample-queries-tpch/tpch_query$i.sql;" >> "$OUT_DIR/query$i.log" 2>&1
		AFTER=$(date +"%s")
		DIFF_SEC=$(($AFTER-$BEFORE))
		echo -e "TPC-H query $i:\t"$(tail -n 10 "$OUT_DIR/query$i.log" | grep Fetched) >> $OUT_DIR/res-log.out
		echo -e "TPC-H query\t$i\tTime To Return\t$DIFF_SEC" >> $RES	
	done
	getstats $TPCH_START $(date +%s) "tpch-allqueries"

	#	Delete database
	hive -e "DROP DATABASE IF EXISTS tpch_flat_orc_$HIVE_SCALE CASCADE;" >> $OUT

	#	Back to 'benchmark-home', remove hive-testbench
	cd ..
	rm -rf hive-testbench >> $OUT
fi

#---SWIM--------------------------------------------------------------------------------------------
#	Setup
#	NB: SWIM compilation requires Java 7!
if ($RUN_SWIM); then
	echo -e $(timestamp)"\tINFO\tStarting SWIM Benchmark" >> $RES

	git clone https://github.com/SWIMProjectUCB/SWIM.git swim >> $OUT 2>&1
	cd swim/workloadSuite

	cp randomwriter_conf.xsl randomwriter_conf.xsl.original >> $OUT 2>&1
	cp workGenKeyValue_conf.xsl workGenKeyValue_conf.xsl.original >> $OUT 2>&1

	# 	Step 1
	#	Usage:
	#   java GenerateReplayScript
	#       [path to synthetic workload file]									
	#       [number of machines in the original production cluster]
	#       [number of machines in the cluster where the workload will be run]
	#       [size of each input partition in bytes]
	#       [number of input partitions]										->	10
	#       [output directory for the scripts]									->	asbench_scriptsTest
	#       [HDFS directory for the input data]									->	/user/$USER/benchmarks/swim
	#       [prefix to workload output in HDFS]									->	swim_
	#       [amount of data per reduce task in byptes]							->	67108864 (std block size)
	#       [workload stdout stderr output dir]									->	$OUT_DIR/swimout
	#       [hadoop command]
	#       [path to WorkGen.jar]
	#       [path to workGenKeyValue_conf.xsl] 
	NO_INPUT_PART_PER_NODE=250
	INPUT_PARTITION_SIZE=67108864
	NO_INPUT_PART=$(($NO_INPUT_PART_PER_NODE * $NO_MACHINES))

	mkdir -p $OUT_DIR/swimout >> $OUT 2>&1
	javac GenerateReplayScript.java

	if [ -z "$LIMIT_SWIM_LINES" ]; then
		# No limitation given
		java GenerateReplayScript FB-2009_samples_24_times_1hr_0.tsv 600 $NO_MACHINES $INPUT_PARTITION_SIZE $(($NO_INPUT_PART_PER_NODE * $NO_MACHINES)) asbench_scriptsTest /user/$USER/benchmarks/swim swim_ 67108864 $OUT_DIR/swimout hadoop WorkGen.jar workGenKeyValue_conf.xsl >> $OUT 2>&1
	else
		head -n $LIMIT_SWIM_LINES FB-2009_samples_24_times_1hr_0.tsv > FB-2009_samples_24_times_1hr_0_limited.tsv
		java GenerateReplayScript FB-2009_samples_24_times_1hr_0_limited.tsv 600 $NO_MACHINES $INPUT_PARTITION_SIZE $(($NO_INPUT_PART_PER_NODE * $NO_MACHINES)) asbench_scriptsTest /user/$USER/benchmarks/swim swim_ 67108864 $OUT_DIR/swimout hadoop WorkGen.jar workGenKeyValue_conf.xsl >> $OUT 2>&1
	fi

	# 	Step 3: Compile Jobs
	mkdir hdfsWrite
	javac -classpath ${HADOOP_HOME}/\*:${HADOOP_HOME}-mapreduce/\* -d hdfsWrite HDFSWrite.java
	jar -cvf HDFSWrite.jar -C hdfsWrite/ .

	mkdir workGen
	javac -classpath ${HADOOP_HOME}/\*:${HADOOP_HOME}-mapreduce/\* -d workGen WorkGen.java
	jar -cvf WorkGen.jar -C workGen/ .


	#	Step 4: Running Workload
	sed -i '/<name>test.randomwrite.bytes_per_map<\/name>/!b;n;c\     <value>'$INPUT_PARTITION_SIZE'</value>' randomwriter_conf.xsl

	TOTAL_BYTES=$(($NO_INPUT_PART * $INPUT_PARTITION_SIZE))
	sed -i '/<name>test.randomwrite.total_bytes<\/name>/!b;n;c\     <value>'$TOTAL_BYTES'</value>' randomwriter_conf.xsl

	hadoop jar HDFSWrite.jar org.apache.hadoop.examples.HDFSWrite -conf randomwriter_conf.xsl /user/$USER/benchmarks/swim >> $OUT

	#	Move to scripts directory
	cd asbench_scriptsTest

	#	create links to required jar
	ln ../WorkGen.jar WorkGen.jar
	ln ../workGenKeyValue_conf.xsl workGenKeyValue_conf.xsl

	SWIM_START=$(date +%s)
	./run-jobs-all.sh >> $OUT
	getstats $SWIM_START $(date +%s) "swim"

	#---SWIM Results reporting & cleanup
	cd ../../..

	echo -e $(timestamp)"\tINFO\tSWIM Benchmark terminated, summarizing results." >> $RES
	i=0
	while [ -f $OUT_DIR/swimout/job-$i.txt ]; do
		echo -e -n "job $i\t" >> $RES
		cat $OUT_DIR/swimout/job-$i.txt | grep "The job took" | awk '{print $4,"\t",$5}' >> $RES
		let i=$i+1
	done

	#	Remove compiled results.
	rm -rf swim
	hdfs dfs -rm -r -skipTrash /user/$USER/benchmarks/swim
fi

#---Cleanup-----------------------------------------------------------------------------------------
echo -e $(timestamp)"\tEVENT\tBENCHMARKS DONE, let's get some beer...'" >> $OUT
#	Rename result folder for subsequent jobs
mv $HOME/$FOLDER $HOME/$(hostname -f)"_"$(date +%Y-%m-%d_%H-%M)"_$FOLDER" >> $OUT

#	Delete possibly remaining folders from DFS
hdfs dfs -rm -r -skipTrash /user/$USER/benchmarks/* >> $OUT
hdfs dfs -rm -r -skipTrash /benchmarks/* >> $OUT

exit 0

