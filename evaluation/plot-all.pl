
#################################################
#  General Configuration Settings				#
#################################################

reset
set term postscript eps enhanced color "Helvetica" 18
set size 1.4,0.85
set grid


set border 3 front linetype -1 linewidth 1.000

set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics


set style histogram columnstacked title  offset character 0, 0, 0

set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autofreq 
set ztics border in scale 0,0 nomirror norotate  offset character 0, 0, 0 autofreq 
set cbtics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autofreq 

#blau: #0000FF, grün: #6B8E23, rot: #FF0000
set style line 1 lt 1 lw 4 linecolor rgb "#FF0000"#rot
set style line 2 lt 2 lw 4 linecolor rgb "#FF0000"#rot 
set style line 3 lt 7 lw 4 linecolor rgb "#FF0000"#rot    
set style line 4 lt 1 lw 4 linecolor rgb "#6B8E23"#grün
set style line 5 lt 2 lw 4 linecolor rgb "#6B8E23"#grün
set style line 6 lt 7 lw 4 linecolor rgb "#6B8E23"#grün
set style line 7 lt 1 lw 4 linecolor rgb "#0000FF"#blau 
set style line 8 lt 2 lw 4 linecolor rgb "#0000FF"#blau 
set style line 9 lt 7 lw 4 linecolor rgb "#0000FF"#blau
set style line 10 lt 1 lw 4 linecolor rgb "#FFA500"#orange
set style line 11 lt 2 lw 4 linecolor rgb "#FFA500"#orange
set style line 12 lt 7 lw 4 linecolor rgb "#FFA500"#orange 
set style line 13 lt 1 lw 4 linecolor rgb "#ff1493"#magenta
set style line 14 lt 2 lw 4 linecolor rgb "#ff1493"#magenta
set style line 15 lt 7 lw 4 linecolor rgb "#ff1493"#magenta
set style line 16 lt 1 lw 4 linecolor rgb "#7FFF00"#chartreuse
set style line 17 lt 2 lw 4 linecolor rgb "#7FFF00"#chartreuse
set style line 18 lt 7 lw 4 linecolor rgb "#7FFF00"#chartreuse
set style line 19 lt 1 lw 4 linecolor rgb "#1E90FF"#dodgerblue
set style line 20 lt 2 lw 4 linecolor rgb "#1E90FF"#dodgerblue
set style line 21 lt 7 lw 4 linecolor rgb "#1E90FF"#dodgerblue

set style line 30 lt 1 lw 4 linecolor rgb "#000000"#black

et style arrow 3 head nofilled size screen 0.02,30 ls 8

##################################################
##    Plot Settings  							##							  
##################################################
set xtics axis (500, 1000, 5000)

set xrange [ : ]
set yrange [ : ]

##################################################
##    Variable definitions						##							  
##################################################

# Initial value for offset. If modifications are required, adapt the value
# at the specific plots.
OFFSET = 0.6

set key inside right bottom #vertical right #reverse enhanced autotitles


#--------------------------------------
###	TestDFSIO read Throughput
#--------------------------------------
set title "TestDFSIO Read Throughput"
set output "out/single-benchmarks/dfsio_read_throughput.ps"
set ylabel "Throughput [mb/sec]"
set xlabel "File size [GB]"

set boxwidth 0.1 absolute
set key inside left top
set style fill pattern

set logscale x
set xtics axis auto

set xrange[0.5: 1400]

OFFSET=0.05
plot "plot-data/dfsio/minicluster-read-throughput.dat" u ($1-2*$1*OFFSET):2 w lines ls 1 t "Mini-Cluster", "" u ($1-2*OFFSET*$1):2:5:6 w errorbars ls 1 notitle,\
"plot-data/dfsio/daplab-read-throughput.dat" u ($1-$1*OFFSET):2 w lines ls 4 t "Daplab", "" u ($1-OFFSET*$1):2:5:6 w errorbars ls 4 notitle,\
"plot-data/dfsio/nutanix-read-throughput.dat" u ($1+$1*OFFSET):2 w lines ls 7 t "Nutanix", "" u ($1+OFFSET*$1):2:5:6 w errorbars ls 7 notitle,\
"plot-data/dfsio/isi3-read-throughput.dat" u ($1+2*$1*OFFSET):2 w lines ls 10 t "Isilon-3", "" u ($1+2*OFFSET*$1):2:5:6 w errorbars ls 10 notitle,\
"plot-data/dfsio/isi6-read-throughput.dat" u 1:2 w lines ls 13 t "Isilon-6", "" u 1:2:5:6 w errorbars ls 13 notitle,\
"plot-data/dfsio/vspex-read-throughput.dat" u 1:2 w lines ls 16 t "VSPEX", "" u 1:2:5:6 w errorbars ls 16 notitle


#--------------------------------------
###	TestDFSIO write Throughput
#--------------------------------------
set title "TestDFSIO Write Throughput"
set output "out/single-benchmarks/dfsio_write_throughput.ps"
set ylabel "Throughput [mb/sec]"
set xlabel "File size [GB]"

set boxwidth 0.1 absolute
set key inside left top
set style fill pattern

set logscale x
set xtics axis auto

set xrange[0.5: 1400]

OFFSET=0.05
plot "plot-data/dfsio/minicluster-write-throughput.dat" u ($1-2*$1*OFFSET):2 w lines ls 1 t "Mini-Cluster", "" u ($1-2*OFFSET*$1):2:5:6 w errorbars ls 1 notitle,\
"plot-data/dfsio/daplab-write-throughput.dat" u ($1-$1*OFFSET):2 w lines ls 4 t "Daplab", "" u ($1-OFFSET*$1):2:5:6 w errorbars ls 4 notitle,\
"plot-data/dfsio/nutanix-write-throughput.dat" u ($1+$1*OFFSET):2 w lines ls 7 t "Nutanix", "" u ($1+OFFSET*$1):2:5:6 w errorbars ls 7 notitle,\
"plot-data/dfsio/isi3-write-throughput.dat" u ($1+2*$1*OFFSET):2 w lines ls 10 t "Isilon-3", "" u ($1+2*OFFSET*$1):2:5:6 w errorbars ls 10 notitle,\
"plot-data/dfsio/isi6-write-throughput.dat" u 1:2 w lines ls 13 t "Isilon-6", "" u 1:2:5:6 w errorbars ls 13 notitle,\
"plot-data/dfsio/vspex-write-throughput.dat" u 1:2 w lines ls 16 t "VSPEX", "" u 1:2:5:6 w errorbars ls 16 notitle


#--------------------------------------
###	Isilon scale: DFSIO write
#--------------------------------------
set title "Isilon Scale Factor: TestDFSIO Write"
set output "out/single-benchmarks/isi-scale-dfsio-write.ps"
set ylabel "Throughput [mb/sec]"
set xlabel "Cluster Size [No. Nodes]"

set key inside left top

unset logscale x
set xtics axis ("3" 3,"4" 4,"5" 5,"6" 6)

set xrange[2.5:6.5]
set yrange[0:*]

OFFSET=0.05
plot "plot-data/dfsio/isi-scale-dfsio-write.dat" u 1:2 w lines ls 1 t "1000 x 1 GB", "" u 1:2:3 w errorbars ls 1 notitle,\
"" u 1:4 w lines ls 4 t "100 x 10 GB", "" u 1:4:5 w yerrorb ls 4 notitle,\
"" u 1:6 w lines ls 7 t "10 x 100 GB", "" u 1:6:7 w errorbars ls 7 notitle,\


#--------------------------------------
###	Isilon scale: DFSIO read
#--------------------------------------
set title "Isilon Scale Factor: TestDFSIO Read"
set output "out/single-benchmarks/isi-scale-dfsio-read.ps"
set ylabel "Throughput [mb/sec]"
set xlabel "Cluster Size [No. Nodes]"

set key inside left top

plot "plot-data/dfsio/isi-scale-dfsio-read.dat" u 1:2 w lines ls 1 t "1000 x 1 GB", "" u 1:2:3 w errorbars ls 1 notitle,\
"" u 1:4 w lines ls 4 t "100 x 10 GB", "" u 1:4:5 w yerrorb ls 4 notitle,\
"" u 1:6 w lines ls 7 t "10 x 100 GB", "" u 1:6:7 w errorbars ls 7 notitle,\



#--------------------------------------
###	NN TPS all
#--------------------------------------
set title "NNBench, TPS"
set output "out/single-benchmarks/nn_tps.ps"
set ylabel "TPS"
set xlabel "Operation"

set boxwidth 0.1 absolute
set key inside left top
set style fill pattern

set xrange [0:*]
unset logscale x
set xtics auto #("Create/Write/Close" 1, "Open" 2, "Rename" 3, "Delete" 4)

OFFSET=0.12
plot "plot-data/nn_tps.dat" using ($2-2*OFFSET):3 title "Mini-Cluster" with boxes fillstyle pattern 0 ls 1,\
"" u ($2-OFFSET):4 title "Nutanix" with boxes fillstyle pattern 1 ls 7,\
"" u ($2):5 title "Isilon-3" with boxes fillstyle pattern 2 ls 4,\
"" u ($2+OFFSET):8 title "VSPEX" with boxes fillstyle pattern 3 ls 10,\
"" u ($2+2*OFFSET):7 title "Daplab" with boxes fillstyle pattern 4 ls 19,\
"" using 2:2:xticlabels(1) notitle with points pointtype -1 



#--------------------------------------
###	NN exec time
#--------------------------------------
set title "NNBench Execution Time"
set output "out/single-benchmarks/nn_exectime.ps"
set ylabel "Avg. Execution Time [ms]"

set boxwidth 0.1 absolute
set key inside left top
set style fill pattern

OFFSET=0.12
plot "plot-data/nn_tps.dat" using ($2-2*OFFSET):3 title "Mini-Cluster" with boxes fillstyle pattern 0 ls 1,\
"" u ($2-OFFSET):4 title "Nutanix" with boxes fillstyle pattern 1 ls 7,\
"" u ($2):5 title "Isilon-3" with boxes fillstyle pattern 2 ls 4,\
"" u ($2+OFFSET):8 title "VSPEX" with boxes fillstyle pattern 3 ls 10,\
"" u ($2+2*OFFSET):7 title "Daplab" with boxes fillstyle pattern 4 ls 19,\
"" using 2:2:xticlabels(1) notitle with points pointtype -1


#--------------------------------------
###	NN Latency
#--------------------------------------
set title "NNBench Latency"
set output "out/single-benchmarks/nn_latency.ps"
set ylabel "Avg. Latency [ms]"

set boxwidth 0.1 absolute
set key inside right top
set style fill pattern

OFFSET=0.13
set tics scale 0
plot "plot-data/nn_latency.dat" using ($2-2*OFFSET):3 title "Mini-Cluster" with boxes fillstyle pattern 0 ls 1,\
"" u ($2-OFFSET):4 title "Nutanix" with boxes fillstyle pattern 1 ls 7,\
"" u ($2):5 title "Isilon-3" with boxes fillstyle pattern 2 ls 4,\
"" u ($2+OFFSET):7 title "Daplab" with boxes fillstyle pattern 4 ls 19,\
"" u ($2+2*OFFSET):8 t "VSPEX" w boxes fillstyle pattern 5 ls 10,\
"" using 2:2:xticlabels(1) notitle with points pointtype -1


#--------------------------------------
### MRBench
#--------------------------------------
set title "MRBench"
set output "out/single-benchmarks/mrbench.ps"
set ylabel "Avg execution time [s]"
unset xlabel

set yrange[0:*]

set boxwidth 0.15 absolute
set key inside left top
set style fill pattern

plot "plot-data/mr.dat" using 2:($3/1000):xtic(1) notitle with boxes fillstyle pattern 6 ls 1

#--------------------------------------
### TeraSort time
#--------------------------------------
set title "Tera Sort (100 GB)"
set output "out/single-benchmarks/terasort.ps"
set ylabel "Time [min]"


set boxwidth 0.15 absolute
set key inside left top
set style fill pattern

plot "plot-data/ts.dat" using 2:3:xtic(1) notitle with boxes fillstyle pattern 6 ls 1,\
					'' u 2:3:4:5 notitle w yerrorb ls 30

#--------------------------------------
### TeraSort2 time - TeraGen
#--------------------------------------
set title "TeraGen (100 GB)"
set output "out/single-benchmarks/ts2-gen.ps"
set ylabel "Time [min]"


set boxwidth 0.15 absolute
set key inside left top
set style fill pattern

plot "plot-data/ts2-gen.dat" using 2:3:xtic(1) notitle with boxes fillstyle pattern 6 ls 1,\
					'' u 2:3:4:5 notitle w yerrorb ls 30

#--------------------------------------
### TeraSort2 time - TeraSort
#--------------------------------------
set title "TeraSort (100 GB)"
set output "out/single-benchmarks/ts2-sort.ps"
set ylabel "Time [min]"


set boxwidth 0.15 absolute
set key inside left top
set style fill pattern

plot "plot-data/ts2-sort.dat" using 2:3:xtic(1) notitle with boxes fillstyle pattern 6 ls 1,\
					'' u 2:3:4:5 notitle w yerrorb ls 30

#--------------------------------------
### TeraSort2 time - TeraValidate
#--------------------------------------
set title "TeraValidate (100 GB)"
set output "out/single-benchmarks/ts2-validate.ps"
set ylabel "Time [min]"


set boxwidth 0.15 absolute
set key inside left top
set style fill pattern

plot "plot-data/ts2-validate.dat" using 2:3:xtic(1) notitle with boxes fillstyle pattern 6 ls 1,\
					'' u 2:3:4:5 notitle w yerrorb ls 30

#--------------------------------------
### SWIM average job duration
#--------------------------------------
set title "SWIM Average Job Duration"
set output "out/single-benchmarks/swim.ps"
set ylabel "Job Duration [sec]"

set boxwidth 0.15 absolute
set key inside left top
set style fill pattern
set tics scale 1

plot "plot-data/swim.dat" using 2:4:xtic(1) notitle with boxes fillstyle pattern 6 ls 1,\
					'' u 2:4:5 notitle w yerrorb ls 30


#--------------------------------------
### TPCH Time to return
#--------------------------------------
set title "TPC-H: 2 GB"
set output "out/single-benchmarks/tpch-2.ps"
set ylabel "Time to return [sec]"

set boxwidth 0.15 absolute
set key inside right top
set style fill pattern

set xrange [0.1:22.9]
set xtics axis 1


OFFSET=0.25
plot "plot-data/tpch-2.dat" using ($1-OFFSET):2 title "Daplab" with boxes fillstyle pattern 3 ls 1,\
"" u 1:3 title "Nutanix" w boxes fillstyle pattern 3 ls 4,\
"" u ($1+OFFSET):4 t "Mini-Cluster" w boxes fillstyle pattern 3 ls 7

#------------------------
set title "TPC-H: 4 GB"
set output "out/single-benchmarks/tpch-4.ps"
set ylabel "Time to return [sec]"

set boxwidth 0.15 absolute
set key inside right top
set style fill pattern

plot "plot-data/tpch-4.dat" using ($1-OFFSET):2 title "Daplab" with boxes fillstyle pattern 3 ls 1,\
"" u 1:3 title "Nutanix" w boxes fillstyle pattern 3 ls 4,\
"" u ($1+OFFSET):4 t "Mini-Cluster" w boxes fillstyle pattern 3 ls 7

#------------------------
set title "TPC-H: 8 GB"
set output "out/single-benchmarks/tpch-8.ps"
set ylabel "Time to return [sec]"

set boxwidth 0.15 absolute
set key inside right top
set style fill pattern

set logscale y
set yrange[*:*]


plot "plot-data/tpch-8.dat" using ($1-OFFSET):2 title "Daplab" with boxes fillstyle pattern 3 ls 1,\
"" u 1:3 title "Nutanix" w boxes fillstyle pattern 3 ls 4,\
"" u ($1+OFFSET):4 t "Mini-Cluster" w boxes fillstyle pattern 3 ls 7

#------------------------
set title "TPC-H: 10 GB"
set output "out/single-benchmarks/tpch-10.ps"
set ylabel "Time to return [sec]"

set boxwidth 0.15 absolute
set key inside right top
set style fill pattern

plot "plot-data/tpch-10.dat" using ($1-OFFSET):2 title "Daplab" with boxes fillstyle pattern 3 ls 1,\
"" u 1:3 title "Nutanix" w boxes fillstyle pattern 3 ls 4,\
"" u ($1+OFFSET):4 t "Mini-Cluster" w boxes fillstyle pattern 3 ls 7

#########################################
#		LINE PLOTS						#
#########################################
set title "Pi Benchmark 500'000 digits"
set output "out/analytics/pi-nomaps.ps"
set ylabel "Time [s]"
set xlabel "No. Maps"

set logscale x 
set logscale y 
set xtics axis (2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192)

set xrange [1.8:*]
set yrange[*:*]

set key inside left bottom reverse

plot "plot-data/analytics/pi-nomaps.dat" using 1:2 title "Mini-Cluster" with line ls 1,\
"" u 1:3 title "Daplab" with line ls 4,\
"" u 1:4 title "Nutanix" with line ls 7,\
"" u 1:5 title "Isilon (3)" with line ls 10,\
"" u 1:6 title "VSPEX" w line ls 13

#------------------------------------------------
### SWIM Detail Plot
#------------------------------------------------
set title "SWIM individual Job results"
set output "out/single-benchmarks/swim-detail.ps"
set ylabel "Time [s]"
set xlabel "Job No."

set datafile missing "NaN"

unset logscale x 
set logscale y 
set xtics axis auto

set xrange [*:*]

set key inside left top reverse

plot "plot-data/swim-detail.dat" using 1:2 title "Mini-Cluster" with line ls 1,\
"" u 1:3 title "Daplab" with line ls 4,\
"" u 1:5 title "Isilon (6)" with line ls 10,\
"" u 1:7 t "Nutanix" w line ls 16,\
"" u 1:10 t "VSPEX" w line ls 19#,\
#"" u 1:4 title "Isilon (3)" with line ls 7,\


#--------------------------------------
###	Replication Factor series 100G
#--------------------------------------
set title "Replication Factor: 100 GB"
set output "out/single-benchmarks/rf-series-100g.ps"
set ylabel "Throughput [mb/sec]"
set xlabel "Replication Factor"

set xrange [0.5:9.5]

unset logscale y
set key inside left bottom

plot "plot-data/rf-series/rf-write-throughput-100gb.dat" u 1:2 w lines ls 1 t "Write", "" u 1:2:5:6 w errorbars ls 1 notitle,\
"plot-data/rf-series/rf-read-throughput-100gb.dat" u 1:2 w lines ls 4 t "Read", "" u 1:2:5:6 w errorbars ls 4 notitle

#--------------------------------------
###	Replication Factor series 1T
#--------------------------------------
set title "Replication Factor: 1 TB"
set output "out/single-benchmarks/rf-series-1tb.ps"
set key inside right top reverse

plot "plot-data/rf-series/rf-write-throughput-1tb.dat" u 1:2 w lines ls 1 t "Write", "" u 1:2:5:6 w errorbars ls 1 notitle,\
"plot-data/rf-series/rf-read-throughput-1tb.dat" u 1:2 w lines ls 4 t "Read", "" u 1:2:5:6 w errorbars ls 4 notitle


#--------------------------------------
###	TPCH Dataset size series
#--------------------------------------
set title "Hive TPC-H"
set output "out/single-benchmarks/tpch-volume_9-2-7.ps"
set key inside left top reverse

set xlabel "Dataset size [GB]"
set ylabel "Time To Return [sec]"
set xrange [1.8:12]

set logscale x 
#set logscale y

set xtics (2, 4, 8, 10)

plot "plot-data/tpch-scale-q2.dat" u 1:2 w lines ls 1 t "Daplab Q2",\
"" u 1:3 w lines ls 4 t "Nutanix Q2",\
"" u 1:4 w lines ls 7 t "Mini-Cluster Q2",\
"plot-data/tpch-scale-q7.dat" u 1:2 w lines ls 2 t "Daplab Q7",\
"" u 1:3 w lines ls 5 t "Nutanix Q7",\
"" u 1:4 w lines ls 8 t "Mini-Cluster Q7",\
"plot-data/tpch-scale-q9.dat" u 1:2 w lines ls 3 t "Daplab Q9",\
"" u 1:3 w lines ls 6 t "Nutanix Q9",\
"" u 1:4 w lines ls 9 t "Mini-Cluster Q9"



#########################################
#		METRICS PLOTS					#
#########################################

#--------------------------------------
###	DAS vs Isilon memory usage
#--------------------------------------
set title "Memory Usage: Daplab DAS vs Isilon"
set output "out/metrics/daplab/das-isilon-memory.ps"
unset logscale x 
unset logscale y
set xrange [*:2600]
set xtics axis auto

set ylabel "Memory [GB]"
set xlabel "Time [s]"
set xtics axis auto

plot "plot-data/metrics/dap-1000x1/csv/testdfsio-read-3/memory/memory_Use.csv" u 1:($2/1024000) title "DAS Read" w lines ls 1,\
"plot-data/metrics/dap-1000x1/csv/testdfsio-write-3/memory/memory_Use.csv" u 1:($2/1024000) title "DAS Write" w lines ls 2,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-read-3/memory/memory_Use.csv" u 1:($2/1024000) title "Isilon Read" w lines ls 4,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-write-3/memory/memory_Use.csv" u 1:($2/1024000) title "Isilon Write" w lines ls 5

#--------------------------------------
###	DAS vs Isilon sys load 1-min
#--------------------------------------
set title "System Load 1-min: Daplab DAS vs Isilon"
set output "out/metrics/daplab/das-isilon-load-1min.ps"


set xrange [*:*]
set yrange [*:50]
set xtics axis auto


set ylabel "System Load Factor"
set xlabel "Time [s]"



plot "plot-data/metrics/dap-1000x1/csv/testdfsio-write-3/load/load_1-min.csv" u 1:2 title "DAS write" w lines ls 1,\
"plot-data/metrics/dap-1000x1/csv/testdfsio-read-3/load/load_1-min.csv" u 1:2 title "DAS read" w lines ls 2,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-write-3/load/load_1-min.csv" u 1:2 title "Isilon write" w lines ls 4,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-read-3/load/load_1-min.csv" u 1:2 title "Isilon read" w lines ls 5

#--------------------------------------
###	DAS vs Isilon sys load 1-min
#--------------------------------------
set title "System Load 1-min: Daplab DAS vs Isilon"
set output "out/metrics/daplab/das-isilon-load-1min.ps"


set xrange [*:*]
set yrange [*:50]
set xtics axis auto

set ylabel "System Load Factor"
set xlabel "Time [s]"

plot "plot-data/metrics/dap-1000x1/csv/testdfsio-write-3/load/load_1-min.csv" u 1:2 title "DAS write" w lines ls 1,\
"plot-data/metrics/dap-1000x1/csv/testdfsio-read-3/load/load_1-min.csv" u 1:2 title "DAS read" w lines ls 2,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-write-3/load/load_1-min.csv" u 1:2 title "Isilon write" w lines ls 4,\
"plot-data/metrics/isi-1000x1/csv/testdfsio-read-3/load/load_1-min.csv" u 1:2 title "Isilon read" w lines ls 5


#--------------------------------------
###	Isi gather plot: TestDFSIO isi6 Network
#--------------------------------------
set title "TestDFSIO Network Throughput Isilon-6"
set output "out/metrics/isilon/dfsio-isi6-net.ps"
set key inside right top noreverse

set xrange [*:*]
set yrange [*:*]
set xtics axis auto

set ylabel "Network Bandwith [GB/s]"
set xlabel "Time [min]"

set datafile separator ","

plot "plot-data/metrics/isi6-testdfsio-allruns-allcluster.dat" u (($1-1440346385)/60):($11/1000000000) title "Network Inbound" w lines ls 4,\
"" u (($1-1440346385)/60):($12/1000000000) title "Network Outbound" w lines ls 7,\


#--------------------------------------
###	Isi gather plot: TestDFSIO isi6 CPU
#--------------------------------------
set title "TestDFSIO CPU Load Isilon-6"
set output "out/metrics/isilon/dfsio-isi6-cpu.ps"
unset key

set xrange [*:*]
set yrange [*:*]
set xtics axis auto

set ylabel "Load [%]"
set xlabel "Time [min]"
plot "plot-data/metrics/isi6-testdfsio-allruns-allcluster.dat" u (($1-1440346385)/60):3 title "CPU Load" w lines ls 1



#--------------------------------------
###	Isi gather plot: TS2 Isi3 Network
#--------------------------------------
set title "TeraSort Network Throughput Isilon-3"
set output "out/metrics/isilon/ts2-isi3-network.ps"
set key inside left top reverse

set xrange [*:*]
set yrange [0:6]
set xtics axis auto
set arrow from 24.5,0 to 24.5,6 nohead lc rgb "#808080"
set arrow from 46.8,0 to 46.8,6 nohead lc rgb "#808080"

set ylabel "Throughput [GB/s]"
set xlabel "Time [min]"
plot "plot-data/metrics/isi3-ts2-allnodes.dat" u (($1-1441724990)/60):($11/1000000000) title "Network Inbound" w lines ls 1,\
"" u (($1-1441724990)/60):($12/1000000000) title "Network Outbound" w lines ls 4 

#--------------------------------------
###	Isi gather plot: TS2 Isi3 CPU
#--------------------------------------
set title "TeraSort CPU Load Isilon-3"
set output "out/metrics/isilon/ts2-isi3-cpu.ps"
unset key

set xrange [*:*]
set yrange [0:100]
set xtics axis auto

unset arrow
set arrow from 24.5,0 to 24.5,100 nohead lc rgb "#808080"
set arrow from 46.8,0 to 46.8,100 nohead lc rgb "#808080"


set ylabel "Load [%]"
set xlabel "Time [min]"
plot "plot-data/metrics/isi3-ts2-allnodes.dat" u (($1-1441724990)/60):3 title "CPU Load" w lines ls 1


#--------------------------------------
###	Isi gather plot: TS2 Isi6 Network
#--------------------------------------
set title "TeraSort Network Throughput Isilon-6"
set output "out/metrics/isilon/ts2-isi6-network.ps"
set key inside left top reverse

set xrange [*:*]
set yrange [0:6]
set xtics axis auto

unset arrow
set arrow from 23,0 to 23,6 nohead lc rgb "#808080"
set arrow from 45.1,0 to 45.1,6 nohead lc rgb "#808080"

set ylabel "Throughput [GB/s]"
set xlabel "Time [min]"
plot "plot-data/metrics/isi6-ts2-allnodes.dat" u (($1-1441663152)/60):($11/1000000000) title "Network Inbound" w lines ls 1,\
"" u (($1-1441663152)/60):($12/1000000000) title "Network Outbound" w lines ls 4 

#--------------------------------------
###	Isi gather plot: TS2 Isi6 CPU
#--------------------------------------
set title "TeraSort CPU Load Isilon-6"
set output "out/metrics/isilon/ts2-isi6-cpu.ps"
unset key

set xrange [*:*]
set yrange [0:100]
set xtics axis auto

unset arrow
set arrow from 23,0 to 23,100 nohead lc rgb "#808080"
set arrow from 45.1,0 to 45.1,100 nohead lc rgb "#808080"

set ylabel "Load [%]"
set xlabel "Time [min]"
plot "plot-data/metrics/isi6-ts2-allnodes.dat" u (($1-1441663152)/60):3 title "CPU Load" w lines ls 1


unset arrow

#########################################
#		SWIM TRACE HISTOGRAM			#
#########################################
set title "SWIM Trace Data Size"
set output "out/analytics/swim-map-input-bytes.ps"
set xlabel "Data size"
set ylabel "Cumulative Jobs"

set logscale x
unset logscale y

set datafile separator "\t"
set yrange [0: ]
set xrange [1 : ]
set key inside right bottom 
set xtics axis ("KB" 1e3,"MB" 1e6,"GB" 1e9,"TB" 1e12)


### Comparing to full trace
set title "Data size at input/shuffle/output"
set output "out/analytics/swim-compare-traces.ps"
set xlabel "Data size"
set ylabel "CDF"

set logscale x
unset logscale y

set yrange [0:1]
set xrange [1 : ]
set key inside right bottom noreverse
set xtics axis ("KB" 1e3,"MB" 1e6,"GB" 1e9,"TB" 1e12)

TRACE_FACTOR=0.000169664


plot 'analytics/fb-trace-1k.csv' u ($4):(.001) smooth cumulative t 'Map 1k' w line ls 1,\
"analytics/fb-trace-full.csv" u ($4):(TRACE_FACTOR) smooth cumulative t 'Map full' w line ls 2,\
"analytics/fb-trace-1k.csv" u ($5):(.001) smooth cumulative t 'Shuffle 1k' w line ls 4,\
"analytics/fb-trace-full.csv" u ($5):(TRACE_FACTOR) smooth cumulative t 'Shuffle full' w line ls 5,\
"analytics/fb-trace-1k.csv" u ($6):(.001) smooth cumulative t 'Reduce 1k' w line ls 7,\
"analytics/fb-trace-full.csv" u ($6):(TRACE_FACTOR) smooth cumulative t 'Reduce full' w line ls 8,

