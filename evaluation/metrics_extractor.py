#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import csv
import sys
import os
import getopt
import pprint


wdir = ''  # Working directory, process all .dat files in this directory.
subfolder = 'csv/'
outputdir = ''

metrics = ['cpu', 'load', 'network', 'memory']

def parse():
    inputdir=os.path.join(os.getcwd(), wdir)
    for f in os.listdir(inputdir):
        #print "DEBUG WORKING DIR: " + inputdir
        #print os.path.abspath(os.path.join(inputdir,f))
        if f.endswith('.dat'):
            # Create output directory
            outdir = os.path.join(outputdir, f.split('.', 1)[0])
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            with open(os.path.join(inputdir,f)) as json_data:
                data = json.load(json_data)

                for metric in metrics:
                    try:
                        data['metrics'][metric].keys()
                    except KeyError:
                        continue
                    m_out = os.path.join(outdir, metric)
                    if not os.path.exists(m_out):
                        os.mkdir(m_out)
                    # Folder structure is created.

                    for key in data['metrics'][metric].keys():
                        out = os.path.join(m_out, metric + '_' + key)
                        with open(out+'.csv', 'w+') as out_csv:
                            # Particular file is open, write csv data.
                            wr = csv.writer(out_csv, delimiter='\t', quoting=csv.QUOTE_NONNUMERIC)

                            for entry in data['metrics'][metric][key]:

                                # Absolute time is neglected - instead, time offset to first entry is written.
                                wr.writerow((entry[1]-data['metrics'][metric][key][0][1], entry[0]))  # write time in first row,

            os.system('gnuplot -e "dir=\'%s/\'" plot-metrics-single.pl' % outdir)



def main(argv):
    global outputdir, wdir

    try:
        opts, args = getopt.getopt(argv, "hd:", ["wdir="])

    except getopt.GetoptError:
        print 'metrics_extractor.py -d <working directory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'metrics_extractor.py -d <working directory>'
            sys.exit()
        elif opt in ("-d", "--directory"):
            wdir = arg

    print 'Working directory ', wdir
    outputdir = os.path.join(wdir, subfolder)
    print 'Output directory is ', outputdir

    parse()


if __name__ == "__main__":
    main(sys.argv[1:])
