
#################################################
#  General Configuration Settings				#
#################################################

reset
set term postscript eps enhanced color "Helvetica" 18
set size 1.4,0.85
set grid


set border 3 front linetype -1 linewidth 1.000

set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics


set style histogram columnstacked title  offset character 0, 0, 0
set xtics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 


set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autofreq 
set ztics border in scale 0,0 nomirror norotate  offset character 0, 0, 0 autofreq 
set cbtics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autofreq 

#blau: #0000FF, grün: #6B8E23, rot: #FF0000
set style line 1 lt 1 lw 8 linecolor rgb "#FF0000"#rot
set style line 2 lt 2 lw 6 linecolor rgb "#FF0000"#rot 
set style line 3 lt 1 lw 6 linecolor rgb "#FF0000"#rot    
set style line 4 lt 1 lw 8 linecolor rgb "#6B8E23"#grün
set style line 5 lt 2 lw 6 linecolor rgb "#6B8E23"#grün
set style line 6 lt 1 lw 6 linecolor rgb "#6B8E23"#grün
set style line 7 lt 1 lw 8 linecolor rgb "#0000FF"#blau 
set style line 8 lt 2 lw 6 linecolor rgb "#0000FF"#blau 
set style line 9 lt 1 lw 6 linecolor rgb "#0000FF"#blau
set style line 10 lt 1 lw 8 linecolor rgb "#FFA500"#orange
set style line 11 lt 2 lw 6 linecolor rgb "#FFA500"#orange
set style line 12 lt 1 lw 6 linecolor rgb "#FFA500"#orange 
set style line 13 lt 1 lw 8 linecolor rgb "#ff1493"#magenta
set style line 14 lt 2 lw 6 linecolor rgb "#ff1493"#magenta
set style line 15 lt 1 lw 6 linecolor rgb "#ff1493"#magenta
set style line 16 lt 1 lw 8 linecolor rgb "#7FFF00"#chartreuse
set style line 17 lt 2 lw 6 linecolor rgb "#7FFF00"#chartreuse
set style line 18 lt 1 lw 6 linecolor rgb "#7FFF00"#chartreuse
set style line 19 lt 1 lw 8 linecolor rgb "#1E90FF"#dodgerblue
set style line 20 lt 2 lw 6 linecolor rgb "#1E90FF"#dodgerblue
set style line 21 lt 1 lw 6 linecolor rgb "#1E90FF"#dodgerblue


set style arrow 3 head nofilled size screen 0.02,30 ls 8

##################################################
##    Plot Settings  							##							  
##################################################
#set xtics axis ("" 10, "" 20, 30, 60, 90, 120 , 150, 200, 300, 500)
#set xtics axis ("" 10, "" 50, "" 100, 200, 400, 800 , 1200, 1800, 3600)
set xtics axis (500, 1000, 5000)


set xrange [ : ]
set yrange [ : ]

##################################################
##    Variable definitions						##							  
##################################################


# Initial value for offset. If modifications are required, adapt the value
# at the specific plots.
OFFSET = 80


#set key inside left top vertical Left reverse enhanced autotitles 
set key outside right bottom #vertical right #reverse enhanced autotitles


# data columns: 5: values: x Min 1stQuartile Median 3rdQuartile Max 
# the values below draw the boxplots (don't change numbers)
# to draw plot versus the right y axis (y2), use: 'axes x1y2' just before 'with candlesticks'


set xlabel "Time [s]"
set ylabel "Load"

######################################
##	CPU
#####################################

set title "CPU Load"
set output dir."cpu.ps"

set xrange [*:*]
set xtics axis 50

plot dir."cpu/cpu_Idle.csv"   title "Idle" with lines ls 1,\
dir."cpu/cpu_Nice.csv"   title "Nice" with lines ls 4,\
dir."cpu/cpu_System.csv" title "System" with lines ls 7,\
dir."cpu/cpu_User.csv"  title "User" with lines ls 10



######################################
##	Network
######################################

set title "Network Throughput"
set output dir.'network.ps'

set xrange [*:*]
set xtics axis 50

plot dir."network/network_In.csv"   title "In" with lines ls 1,\
dir."network/network_Out.csv"   title "Out" with lines ls 4


######################################
##	Memory
######################################

set title "Memory Usage"
set output dir.'memory.ps'

set xrange [*:*]
set xtics axis 50

plot dir."memory/memory_Buffer.csv"   title "Buffer" with lines ls 1,\
dir."memory/memory_Cache.csv"   title "Cache" with lines ls 4,\
dir."memory/memory_Share.csv"   title "Share" with lines ls 7,\
dir."memory/memory_Swap.csv"   title "Swap" with lines ls 10,\
dir."memory/memory_Total.csv"   title "Total" with lines ls 13,\
dir."memory/memory_Use.csv"   title "Use" with lines ls 17

######################################
##	Load
######################################

set title "Cluser Load coefficients"
set output dir.'load.ps'

set xrange [*:*]
set xtics axis 50

plot dir."load/load_1-min.csv"   title "1-min" with lines ls 1,\
dir."load/load_CPUs.csv"   title "CPUs" with lines ls 4,\
dir."load/load_Procs.csv"   title "Procs" with lines ls 7


